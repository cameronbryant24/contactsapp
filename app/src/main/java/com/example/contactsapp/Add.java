package com.example.contactsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Add extends AppCompatActivity {

    Button btn_add1;
    ImageView iv_pic;
    EditText et_firstName, et_lastName, et_number, et_email, et_address, et_url, et_businessHours, et_birthDay, et_description;

    int positionToEdit = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        btn_add1 = findViewById(R.id.btn_add1);

        iv_pic = findViewById(R.id.iv_pic);

        et_firstName = findViewById(R.id.et_firstName);
        et_lastName = findViewById(R.id.et_lastName);
        et_number = findViewById(R.id.et_number);
        et_email = findViewById(R.id.et_email);
        et_address = findViewById(R.id.et_address);
        et_url = findViewById(R.id.et_url);
        et_businessHours = findViewById(R.id.et_businessHours);
        et_birthDay = findViewById(R.id.et_birthDay);
        et_description = findViewById(R.id.et_description);

        Bundle incomingIntent = getIntent().getExtras();

        if (incomingIntent != null) {
            String firstName = incomingIntent.getString("first name");
            String lastName = incomingIntent.getString("last name");
            int number = incomingIntent.getInt("number");
            String email = incomingIntent.getString("email");
            String address = incomingIntent.getString("address");
            String url = incomingIntent.getString("url");
            String businessHours = incomingIntent.getString("business hours");
            String birthDay = incomingIntent.getString("birth day");
            String description = incomingIntent.getString("description");
            positionToEdit = incomingIntent.getInt("edit");
        }

        btn_add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newFirstName = et_firstName.getText().toString();
                String newLastName = et_lastName.getText().toString();
                String newNumber = et_number.getText().toString();
                String newEmail = et_email.getText().toString();
                String newAddress = et_address.getText().toString();
                String newUrl = et_url.getText().toString();
                String newBusinessHours = et_businessHours.getText().toString();
                String newBirthDay = et_birthDay.getText().toString();
                String newDescription = et_description.getText().toString();

                Intent i = new Intent(v.getContext(), Address.class);

                i.putExtra("edit", positionToEdit);
                i.putExtra("first name", newFirstName);
                i.putExtra("last name", newLastName);
                i.putExtra("number", newNumber);
                i.putExtra("email", newEmail);
                i.putExtra("address", newAddress);
                i.putExtra("url", newUrl);
                i.putExtra("business hours", newBusinessHours);
                i.putExtra("birth day", newBirthDay);
                i.putExtra("description", newDescription);

                startActivity(i);
            }
        });

    }
}
