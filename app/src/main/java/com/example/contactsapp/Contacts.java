package com.example.contactsapp;

import java.util.ArrayList;
import java.util.List;

public class Contacts {

    List<Person> myContacts;

    public Contacts(List<Person> myContacts) {
        this.myContacts = myContacts;
    }

    public Contacts() {
        String [] startingNames = {"Alex", "Bennie", "Carly", "Dad", "GCU"};
        this.myContacts = new ArrayList<>();
        String [] startingLastNames = {"Ramis", "Plunket", "Young", "Bryant", "GCU"};
        int [] startingNumbers = {6601111, 6452222, 6143333, 6404444, 6605555};
        String [] startingEmail = {"alex@gmail.com", "BennieP@gmail.com", "CarlyYoung@gmail.com", "DBryant@gmail.com", "GCU@gmail.com"};
        String [] startingAddress = {"Grove lane", "Colter Street", "Colter Street", "Home Street", "Camelback Road"};
        String [] startingUrl = {"alex.com", "Bennie.com", "carly.com", "Dad.com", "gcu.edu"};
        String [] startingBusinessHours = {"NA", "NA", "NA", "NA", "Monday-Sunday 6AM-8PM"};
        String [] startingBirthDay = {"June 8, 2000", "September 27, 1997", "August 13, 1999", "January 9, 1969", "NA"};
        String [] startingDescription = {"Long hair, brown eyes, friend", "Tall, fit, roommate", "Short, blond hair, friend", "Tall, glasses, father", "School"};
        for (int i = 0; i<startingNames.length; i++) {
            Person p = new Person(startingNames[i], startingLastNames[i], startingNumbers[i], startingEmail[i], startingAddress[i], startingUrl[i], startingBusinessHours[i],
                    startingBirthDay[i], startingDescription[i]);
            myContacts.add(p);
        }
    }

    public List<Person> getMyContacts() {
        return myContacts;
    }

    public void setMyContacts(List<Person> myContacts) {
        this.myContacts = myContacts;
    }
}
