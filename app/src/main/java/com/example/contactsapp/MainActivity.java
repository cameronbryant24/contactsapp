package com.example.contactsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    Button btn_add, btn_search;
    ImageView iv_logo;
    PersonAdapter adapter;
    Contacts myContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myContacts = ((MyApp) this.getApplication()).getMyContacts();

        iv_logo = findViewById(R.id.iv_logo);

        btn_add = findViewById(R.id.btn_add);

        adapter = new PersonAdapter(MainActivity.this, myContacts);

        btn_add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), Add.class);
                startActivity(i);
            }
        });

        btn_search = findViewById(R.id.btn_search);

        btn_search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent goToSearch = new Intent(v.getContext(), Search.class);
                startActivity(goToSearch);
            }
        });

    }
}
