package com.example.contactsapp;

import android.app.Application;

public class MyApp extends Application {

    private Contacts myContacts = new Contacts();

    public Contacts getMyContacts() {
        return myContacts;
    }

    public void setMyContacts(Contacts myContacts) {
        this.myContacts = myContacts;
    }
}
