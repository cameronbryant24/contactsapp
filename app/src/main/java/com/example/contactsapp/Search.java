package com.example.contactsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Search extends AppCompatActivity {

    TextView tv_searchWindow;
    Button btn_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        tv_searchWindow = findViewById(R.id.tv_searchWindow);

        btn_address = findViewById(R.id.btn_address);

        btn_address.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent goToAddress = new Intent(v.getContext(), Address.class);
                startActivity(goToAddress);
            }
        });

    }
}
